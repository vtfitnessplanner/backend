import requests
import flask
import logging
import os

from datetime import datetime
from firebase_admin import auth
from dotenv import load_dotenv

load_dotenv()
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.DEBUG)

API_KEY = os.environ.get("FIREBASE_API_KEY")


def test_register(client):
    firebase_login_api = f"https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key={API_KEY}"
    response = requests.post(
        firebase_login_api,
        json={
            "email": "test123@test.com",
            "password": "test123",
            "returnSecureToken": True,
        },
        headers={"Content-Type": "application/json"},
    )
    response.raise_for_status()
    jwt = str(response.json()["idToken"])

    response = client.post(
        "/auth/register",
        headers={"X-Firebase-AppCheck": jwt},
        json={
            "fname": "test",
            "lname": "osterone",
            "DOB": datetime(2000, 1, 1),
            "gender": "m",
        },
    )
    assert response.status_code == 200
