import os
import tempfile
import pytest
from dotenv import load_dotenv
from backend import create_app


@pytest.fixture
def app():
    load_dotenv()
    app = create_app("test")
    return app


@pytest.fixture
def client(app):
    return app.test_client()
