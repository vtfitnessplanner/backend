# Backend

This is the backend for our project.

## Requirements

- Python 3.12 >

## Running Instructions

Before running the backend, make sure to execute the command `pip install -r requirements.txt` that will install the backend requirements.

### Environment Variables

To run the backend the following environment variables need to be set.

- FIREBASE_PROJECT_ID
- FIREBASE_PRIVATE_KEY_ID
- FIREBASE_PRIVATE_KEY
- FIREBASE_CLIENT_EMAIL
- FIREBASE_CLIENT_ID
- FIREBASE_CLIENT_CERT_URL
- SQLALCHEMY_DATABASE_URI

Additionally, when testing, the following variables must be specified:

- FIREBASE_API_KEY

## Route Descriptions

- ".../auth/register":
  Handles register POST requests.
  A register call must be of the following format:

  Must contain the following headers:

  - X-Firebase-AppCheck: JWT token provided by firebase

  Must contain a body with the following attributes:

  - fname(str): first name
  - lname(str): last name
  - DOB(Date): user's birthday
  - gender(str): user's gender

###
