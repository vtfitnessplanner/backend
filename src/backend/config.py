import os


class ProdConfig:
    # Database configuration
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")


class DevConfig:
    # Database configuration
    SQLALCHEMY_DATABASE_URI = (
        "mysql+mysqlconnector://root:password@localhost:3306/vtmealplandb"
    )


class TestConfig:
    # Database configuration

    SQLALCHEMY_DATABASE_URI = (
        "mysql+mysqlconnector://root:password@localhost:3306/vtmealplandb"
    )

    TEST = True


config = {"dev": DevConfig, "prod": ProdConfig, "test": TestConfig}
