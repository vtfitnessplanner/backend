from flask import Flask
from typing import Optional

from . import database
from . import config


def register_extensions(app):
    database.db.init_app(app)
    with app.app_context():
        database.db.create_all()

    from .auth import bp_auth

    app.register_blueprint(bp_auth)


def create_app(app_environment: Optional[str] = None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    if app_environment is not None:
        app.config.from_object(config.config[app_environment])
        print(f"Loading {app_environment} config.")
    else:
        app.config.from_object(config.config["dev"])
        print("Loading dev config.")

    register_extensions(app)
    return app
