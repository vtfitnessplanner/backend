from dateutil.parser import parse
import firebase_admin  # type: ignore
import flask
import jwt
import logging
import functools
import os

from firebase_admin import auth
from typing import Callable

from .database import User, db

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.DEBUG)

cred_dict = {
    "type": "service_account",
    "project_id": os.environ.get("FIREBASE_PROJECT_ID"),
    "private_key_id": os.environ.get("FIREBASE_PRIVATE_KEY_ID"),
    "private_key": os.environ.get("FIREBASE_PRIVATE_KEY", "").replace(r"\n", "\n"),
    "client_email": os.environ.get("FIREBASE_CLIENT_EMAIL"),
    "client_id": os.environ.get("FIREBASE_CLIENT_ID"),
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": os.environ.get("FIREBASE_CLIENT_CERT_URL"),
    "universe_domain": "googleapis.com",
}

cred = firebase_admin.credentials.Certificate(cred_dict)
firebase_app = firebase_admin.initialize_app(cred)

bp_auth = flask.Blueprint("auth", __name__, url_prefix="/auth")


def login_required(f) -> Callable:
    @functools.wraps(f)
    def inner(*args, **kwargs):
        app_check_token = flask.request.headers.get("X-Firebase-AppCheck")
        if app_check_token is None:
            return "No token provided in the header", 400
        try:
            app_check_claims = auth.verify_id_token(app_check_token)
            flask.g.firebase_claims = app_check_claims
            f(*args, **kwargs)

        except (ValueError, jwt.exceptions.DecodeError):
            return "Failed to authenticate", 401

        except Exception as e:
            LOGGER.error(e)
            return "Unknown error", 500

    return inner


@bp_auth.route("/register", methods=["POST"])
def register():
    """
    Handles register POST requests.
    A register must be of the following format:
        Must contain a header X-Firebase-AppCheck
        Must contain a body with the following attributes:
            fname(str): first name
            lname(str): last name
            DOB(Date): user's birthday
            gender(str): user's gender

    """
    LOGGER.info("Verifying request has a valid format.")

    body = flask.request.json

    if body is None:
        return "Missing body.", 400

    for key in ["fname", "lname", "DOB", "gender"]:
        if key not in body.keys():
            return f"Missing {key} in the body.", 400

    app_check_token = flask.request.headers.get("X-Firebase-AppCheck")

    if app_check_token is None:
        return "Missing header", 400

    LOGGER.info("Trying to decode the JWT Token")
    try:
        uid = auth.verify_id_token(app_check_token)["uid"]
        user_record = auth.get_user(uid)

    except auth.UserNotFoundError as e:
        return "Provided UID does not correspond to any user", 409

    LOGGER.info("Verifying that the user does not exist.")
    user_email = user_record.email
    db_user_results = db.session.query(User).filter(User.userEmail == user_email)
    if db_user_results.count() >= 1:
        return (
            f"{db_user_results.count()} already exist in the database. Can't register more.",
            409,
        )

    LOGGER.info("Pushing to the database.")
    new_user = User(
        userFName=body["fname"],
        userLName=body["lname"],
        userEmail=user_email,
        userDOB=parse(body["DOB"]),
        userGender=body["gender"],
        userAuthToken=uid,
    )  # type: ignore

    db.session.add(new_user)
    db.session.commit()

    return "Registered", 200
