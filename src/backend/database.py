from datetime import datetime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import mapped_column, Mapped
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    __tablename__ = "User"

    userId: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    userFName: Mapped[str] = mapped_column(default=None)
    userLName: Mapped[str] = mapped_column(default=None)
    userEmail: Mapped[str] = mapped_column(default=None)
    userDOB: Mapped[datetime] = mapped_column(default=None)
    userGender: Mapped[str] = mapped_column(default=None)
    userAuthToken: Mapped[str] = mapped_column(default=None)


class WorkoutPlan(db.Model):
    __tablename__ = "WorkoutPlan"

    workoutPlanId: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    workoutType: Mapped[str] = mapped_column(default=None)
    workoutCalorieExpect: Mapped[int] = mapped_column(default=None)


class Food(db.Model):
    __tablename__ = "Food"

    foodId: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    foodName: Mapped[str] = mapped_column(default=None)
    foodCalorie: Mapped[int] = mapped_column(default=None)
    foodFat: Mapped[float] = mapped_column(default=None)
    foodCholesterol: Mapped[float] = mapped_column(default=None)
    foodCarb: Mapped[float] = mapped_column(default=None)
    foodSugar: Mapped[float] = mapped_column(default=None)
    foodProtein: Mapped[float] = mapped_column(default=None)
    foodServingTime: Mapped[str] = mapped_column(default=None)
    foodLocation: Mapped[str] = mapped_column()


class FoodDate(db.Model):
    __tablename__ = "FoodDate"

    foodDateId: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    foodDateDate: Mapped[datetime] = mapped_column(default=None)
    foodId: Mapped[int] = mapped_column(ForeignKey(Food.foodId))


class Meal(db.Model):
    __tablename__ = "Meal"

    mealId: Mapped[int] = mapped_column(primary_key=True)
    foodId: Mapped[int] = mapped_column(ForeignKey(Food.foodId))
    userId: Mapped[int] = mapped_column(ForeignKey(User.userId))


class FintnessStat(db.Model):
    __tablename__ = "FitnessStat"

    fitstatId: Mapped[int] = mapped_column(primary_key=True)
    fitstat_height_inches: Mapped[float] = mapped_column(default=None)
    fitstat_weight_lbs: Mapped[float] = mapped_column(default=None)
    workoutPlanId: Mapped[int] = mapped_column(ForeignKey(WorkoutPlan.workoutPlanId))
    userId: Mapped[int] = mapped_column(ForeignKey(User.userId))
